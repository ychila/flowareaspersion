﻿Imports DevExpress.Web

Public Class DocumentosWFrm
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack = True Then

            dtpFFin.Date = Date.Now
            dtpFini.Date = Date.Now

        End If
    End Sub

    Protected Sub btnCargar_Click(sender As Object, e As EventArgs) Handles btnCargar.Click

        cargar()


    End Sub

    Private Sub cargar()


        Dim tmpFechaInicial As String = ""
        Dim tmpFechaFinal As String = ""


        tmpFechaInicial = Me.dtpFini.Date.Month & "/" & Me.dtpFini.Date.Day & "/" & Me.dtpFini.Date.Year
        tmpFechaFinal = Me.dtpFFin.Date.Month & "/" & Me.dtpFFin.Date.Day & "/" & Me.dtpFFin.Date.Year


        SqlDataSourceCargaDocumentos.ConnectionString = strConexion
        SqlDataSourceCargaDocumentos.SelectCommand = "EXEC sp_GetInfoAspersionDocument " & Me.Session("IDUsr") & ", '" & tmpFechaInicial & "', '" & tmpFechaFinal & "'"
        gvDocumentos.DataBind()

    End Sub

    Private Sub gvDocumentos_CustomButtonCallback(sender As Object, e As ASPxGridViewCustomButtonCallbackEventArgs) Handles gvDocumentos.CustomButtonCallback
        Dim tmpID As Integer = 0
        Dim tmpSel As Integer
        Dim tmpToPrint As Integer

        On Error GoTo ControlaError

        tmpID = Me.gvDocumentos.GetSelectedFieldValues("ID")(0)

        Dim tmpComando As String = ""
        tmpComando = e.ButtonID

        Select Case tmpComando
            Case "Anular"

                Dim strSQL As String = ""

                strSQL = "UPDATE Aspersion_Header SET FlagActivo = 0, IDAppUserAnulo = " & Me.Session("IDUsr") & " WHERE ID = " & tmpID & ""
                GetExecComando(strSQL, strConexion)

                cargar()



        End Select

        Exit Sub

ControlaError:
        Dim strError As String = Err.Description
    End Sub
End Class
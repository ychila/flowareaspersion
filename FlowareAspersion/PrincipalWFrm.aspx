﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PrincipalWFrm.aspx.vb" Inherits="FlowareAspersion.PrincipalWFrm" %>

<%@ Register assembly="DevExpress.Web.v17.1, Version=17.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        #Contenedor
        {
            width: 90%;
            margin: 0 auto;
          
        }
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            width: 35px;
        }
        .auto-style3 {
            width: 179px;
        }
        .auto-style4 {
            width: 52px;
        }
        .auto-style5 {
            width: 162px;
        }
        .auto-style9 {
            text-align: right;
        }
        .auto-style10 {
            width: 26px;
        }
        .auto-style11 {
            width: 80px;
        }
        .auto-style12 {
            width: 227px;
        }
        .auto-style13 {
            width: 59px;
        }
        .auto-style14 {
            text-align: center;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div id="Contenedor">
    
        <table class="auto-style1">
            <tr>
                <td>
                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>
                </td>
            </tr>
            <tr>
                <td class="auto-style9">
                    <dx:ASPxButton ID="btnImportarExcel" runat="server" Text="Importar Excel" Theme="Metropolis">
                    </dx:ASPxButton>
                    <dx:ASPxButton ID="btnDocumento" runat="server" Text="Documentos" Theme="Metropolis" Width="90px">
                    </dx:ASPxButton>
                    <dx:ASPxButton ID="btnImprimirMasa" runat="server" Text="Imprimir" Theme="Metropolis" Width="95px">
                    </dx:ASPxButton>
                    <dx:ASPxButton ID="btnSalir" runat="server" Text="Salir" Theme="Metropolis" Width="95px">
                    </dx:ASPxButton>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/Logo.jpg" />
                </td>
            </tr>
            <tr>
                <td>
                    <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Bienvenido" Theme="BlackGlass">
                    </dx:ASPxLabel>
                    <dx:ASPxLabel ID="lblUsuario" runat="server" Theme="BlackGlass">
                    </dx:ASPxLabel>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <table class="auto-style1">
                        <tr>
                            <td class="auto-style2">
                                <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Finca" Theme="BlackGlass">
                                </dx:ASPxLabel>
                            </td>
                            <td class="auto-style12">
                                <dx:ASPxComboBox ID="cmbCultivo" runat="server" Theme="SoftOrange" ValueType="System.Int32" DataSourceID="SqlDataSourceCultivos" SelectedIndex="0" TextField="Nombre" ValueField="ID" Width="220px">
                                </dx:ASPxComboBox>
                            </td>
                            <td class="auto-style13">
                                <dx:ASPxLabel ID="ASPxLabel6" runat="server" Text="Producto" Theme="SoftOrange">
                                </dx:ASPxLabel>
                            </td>
                            <td class="auto-style3">
                                <dx:ASPxComboBox ID="cmbProducto" runat="server" DataSourceID="SqlDataSourceProductos" EnableTheming="True" SelectedIndex="0" TextField="Producto" Theme="SoftOrange" ValueField="Producto">
                                </dx:ASPxComboBox>
                            </td>
                            <td class="auto-style4">
                                <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="Semana" Theme="BlackGlass">
                                </dx:ASPxLabel>
                            </td>
                            <td class="auto-style11">
                                <dx:ASPxSpinEdit ID="SpeSemana" runat="server" Number="0" Theme="SoftOrange" Width="70px" />
                            </td>
                            <td class="auto-style10">
                                <dx:ASPxLabel ID="ASPxLabel5" runat="server" Text="Dia">
                                </dx:ASPxLabel>
                            </td>
                            <td class="auto-style5">
                                <dx:ASPxComboBox ID="cmbDiaSemana" runat="server" EnableTheming="True" SelectedIndex="0" Theme="SoftOrange" Width="150px">
                                    <Items>
                                        <dx:ListEditItem Selected="True" Text="TODAS" Value="TODAS" />
                                        <dx:ListEditItem Text="LUNES" Value="LUNES" />
                                        <dx:ListEditItem Text="MARTES" Value="MARTES" />
                                        <dx:ListEditItem Text="MIERCOLES" Value="MIERCOLES" />
                                        <dx:ListEditItem Text="JUEVES" Value="JUEVES" />
                                        <dx:ListEditItem Text="VIERNES" Value="VIERNES" />
                                        <dx:ListEditItem Text="SABADO" Value="SABADO" />
                                        <dx:ListEditItem Text="DOMINGO" Value="DOMINGO" />
                                    </Items>
                                </dx:ASPxComboBox>
                            </td>
                            <td>
                                <dx:ASPxButton ID="btnCargar" runat="server" Text="Cargar" Theme="Metropolis" Width="107px">
                                </dx:ASPxButton>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style2">
                                &nbsp;</td>
                            <td class="auto-style12">
                                &nbsp;</td>
                            <td class="auto-style13">
                                &nbsp;</td>
                            <td class="auto-style3">
                                &nbsp;</td>
                            <td class="auto-style4">
                                &nbsp;</td>
                            <td class="auto-style11">
                                &nbsp;</td>
                            <td class="auto-style10">
                                &nbsp;</td>
                            <td class="auto-style5">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:SqlDataSource ID="SqlDataSourceAspersion" runat="server" ConnectionString="<%$ ConnectionStrings:Aspersion %>" SelectCommand="EXEC sp_CargaDatosAspersion @prmCultivo, @prmFecha, @prmDia, @prmProducto">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="cmbCultivo" DefaultValue="-1" Name="prmCultivo" PropertyName="Value" />
                            <asp:ControlParameter ControlID="SpeSemana" DefaultValue="0" Name="prmFecha" PropertyName="Number" />
                            <asp:ControlParameter ControlID="cmbDiaSemana" DefaultValue="" Name="prmDia" PropertyName="Value" />
                            <asp:ControlParameter ControlID="cmbProducto" DefaultValue="TODOS" Name="prmProducto" PropertyName="Value" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSourceProductos" runat="server" ConnectionString="<%$ ConnectionStrings:Aspersion %>" ProviderName="<%$ ConnectionStrings:Aspersion.ProviderName %>" SelectCommand="EXEC sp_CargaProductosAspersion"></asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSourceCultivos" runat="server" ConnectionString="<%$ ConnectionStrings:Aspersion %>" SelectCommand="SELECT ID, Nombre FROM Aspersion_Grow WHERE FlagActivo = 1 AND (ID = @prmCultivo OR @prmCultivo = 0)">
                        <SelectParameters>
                            <asp:SessionParameter DefaultValue="-1" Name="prmCultivo" SessionField="IDAspersionGrow" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    <dx:ASPxPopupControl ID="ASPxPopupControl2" runat="server" HeaderText="Carga de Excel..." PopupAction="None" PopupAnimationType="None" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Theme="SoftOrange" Width="480px">
                        <ContentCollection>
<dx:PopupControlContentControl runat="server">
    <table class="auto-style1">
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="Archivo">
                </dx:ASPxLabel>
            </td>
            <td>
                <asp:FileUpload ID="FileUpload1" runat="server" Font-Names="Segoe UI" Width="227px" />
            </td>
            <td>
                <dx:ASPxButton ID="btnSubirExcel" runat="server" Text="Subir" Theme="Metropolis" Width="107px">
                </dx:ASPxButton>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="6">
                <asp:GridView ID="gvImportExcel" runat="server" AllowPaging="True" Visible="False" Width="100%">
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td colspan="6">&nbsp;</td>
        </tr>
    </table>
                            </dx:PopupControlContentControl>
</ContentCollection>
                        <Border BorderWidth="10px" />
                    </dx:ASPxPopupControl>
                    <dx:ASPxPopupControl ID="ASPxPopupControl3" runat="server" HeaderText="Alerta..." Height="70px" PopupAction="None" PopupAnimationType="None" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Theme="SoftOrange" Width="350px">
                        <ContentCollection>
<dx:PopupControlContentControl runat="server">
    <table class="auto-style1">
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style14">
                <dx:ASPxLabel ID="lblMensajeAlerta" runat="server" Theme="SoftOrange">
                </dx:ASPxLabel>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
    </table>
                            </dx:PopupControlContentControl>
</ContentCollection>
                        <Border BorderWidth="10px" />
                    </dx:ASPxPopupControl>
                    <dx:ASPxGridView ID="GVAspersion" runat="server" EnableCallBacks="False" Theme="SoftOrange" Width="100%" AutoGenerateColumns="False" DataSourceID="SqlDataSourceAspersion" KeyFieldName="ID">
                        <SettingsAdaptivity AdaptivityMode="HideDataCells">
                        </SettingsAdaptivity>
                        <SettingsPager PageSize="25">
                        </SettingsPager>
                        <Settings ShowFooter="True" />
                        <SettingsBehavior AllowSelectByRowClick="True" AllowSelectSingleRowOnly="True" EnableRowHotTrack="True" />
                        <SettingsSearchPanel Visible="True" />
                        <Columns>
                            <dx:GridViewDataTextColumn FieldName="ID" VisibleIndex="1" ReadOnly="True" Visible="False">
                                <EditFormSettings Visible="False" />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Semana" VisibleIndex="2">
                                <Settings AllowHeaderFilter="True" />
                                <SettingsHeaderFilter Mode="CheckedList">
                                </SettingsHeaderFilter>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Dia" VisibleIndex="3">
                                <Settings AllowHeaderFilter="True" />
                                <SettingsHeaderFilter Mode="CheckedList">
                                </SettingsHeaderFilter>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Finca" VisibleIndex="4">
                                <Settings AllowHeaderFilter="True" />
                                <SettingsHeaderFilter Mode="CheckedList">
                                </SettingsHeaderFilter>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Producto" VisibleIndex="5">
                                <Settings AllowHeaderFilter="True" />
                                <SettingsHeaderFilter Mode="CheckedList">
                                </SettingsHeaderFilter>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Bloque" VisibleIndex="6">
                                <Settings AllowHeaderFilter="True" />
                                <SettingsHeaderFilter Mode="CheckedList">
                                </SettingsHeaderFilter>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="PPC" VisibleIndex="7">
                                <Settings AllowHeaderFilter="True" />
                                <SettingsHeaderFilter Mode="CheckedList">
                                </SettingsHeaderFilter>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Cantidad" VisibleIndex="8" Caption="Saldo">
                                <Settings AllowHeaderFilter="True" />
                                <SettingsHeaderFilter Mode="CheckedList">
                                </SettingsHeaderFilter>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Unidad" VisibleIndex="27">
                                <Settings AllowHeaderFilter="True" />
                                <SettingsHeaderFilter Mode="CheckedList">
                                </SettingsHeaderFilter>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="Cat. Tox" FieldName="CategoriaToxicologica" VisibleIndex="28">
                                <Settings AllowHeaderFilter="True" />
                                <SettingsHeaderFilter Mode="CheckedList">
                                </SettingsHeaderFilter>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewCommandColumn ButtonRenderMode="Image" ButtonType="Image" Caption="Imp" VisibleIndex="0">
                                <CustomButtons>
                                    <dx:GridViewCommandColumnCustomButton ID="Imp">
                                        <Image IconID="print_printer_16x16">
                                        </Image>
                                    </dx:GridViewCommandColumnCustomButton>
                                </CustomButtons>
                            </dx:GridViewCommandColumn>
                            <dx:GridViewDataTextColumn Caption="T2" FieldName="Cantidad2" VisibleIndex="9">
                                <Settings AllowHeaderFilter="True" />
                                <SettingsHeaderFilter Mode="CheckedList">
                                </SettingsHeaderFilter>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="T3" FieldName="Cantidad3" VisibleIndex="10">
                                <Settings AllowHeaderFilter="True" />
                                <SettingsHeaderFilter Mode="CheckedList">
                                </SettingsHeaderFilter>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="T4" FieldName="Cantidad4" VisibleIndex="11">
                                <Settings AllowHeaderFilter="True" />
                                <SettingsHeaderFilter Mode="CheckedList">
                                </SettingsHeaderFilter>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="T5" FieldName="Cantidad5" VisibleIndex="12">
                                <Settings AllowHeaderFilter="True" />
                                <SettingsHeaderFilter Mode="CheckedList">
                                </SettingsHeaderFilter>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="T6" FieldName="Cantidad6" VisibleIndex="13">
                                <Settings AllowHeaderFilter="True" />
                                <SettingsHeaderFilter Mode="CheckedList">
                                </SettingsHeaderFilter>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="T7" FieldName="Cantidad7" VisibleIndex="14">
                                <Settings AllowHeaderFilter="True" />
                                <SettingsHeaderFilter Mode="CheckedList">
                                </SettingsHeaderFilter>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="T8" FieldName="Cantidad8" VisibleIndex="15">
                                <Settings AllowHeaderFilter="True" />
                                <SettingsHeaderFilter Mode="CheckedList">
                                </SettingsHeaderFilter>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="T9" FieldName="Cantidad9" VisibleIndex="16">
                                <Settings AllowHeaderFilter="True" />
                                <SettingsHeaderFilter Mode="CheckedList">
                                </SettingsHeaderFilter>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="T10" FieldName="Cantidad10" VisibleIndex="17">
                                <Settings AllowHeaderFilter="True" />
                                <SettingsHeaderFilter Mode="CheckedList">
                                </SettingsHeaderFilter>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="T11" FieldName="Cantidad11" VisibleIndex="18">
                                <Settings AllowHeaderFilter="True" />
                                <SettingsHeaderFilter Mode="CheckedList">
                                </SettingsHeaderFilter>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="T12" FieldName="Cantidad12" VisibleIndex="19">
                                <Settings AllowHeaderFilter="True" />
                                <SettingsHeaderFilter Mode="CheckedList">
                                </SettingsHeaderFilter>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="T13" FieldName="Cantidad13" VisibleIndex="20">
                                <Settings AllowHeaderFilter="True" />
                                <SettingsHeaderFilter Mode="CheckedList">
                                </SettingsHeaderFilter>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="T14" FieldName="Cantidad14" VisibleIndex="21">
                                <Settings AllowHeaderFilter="True" />
                                <SettingsHeaderFilter Mode="CheckedList">
                                </SettingsHeaderFilter>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="T15" FieldName="Cantidad15" VisibleIndex="22">
                                <Settings AllowHeaderFilter="True" />
                                <SettingsHeaderFilter Mode="CheckedList">
                                </SettingsHeaderFilter>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="T16" FieldName="Cantidad16" VisibleIndex="23">
                                <Settings AllowHeaderFilter="True" />
                                <SettingsHeaderFilter Mode="CheckedList">
                                </SettingsHeaderFilter>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="T17" FieldName="Cantidad17" VisibleIndex="24">
                                <Settings AllowHeaderFilter="True" />
                                <SettingsHeaderFilter Mode="CheckedList">
                                </SettingsHeaderFilter>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="T18" FieldName="Cantidad18" VisibleIndex="25">
                                <Settings AllowHeaderFilter="True" />
                                <SettingsHeaderFilter Mode="CheckedList">
                                </SettingsHeaderFilter>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="T19" FieldName="Cantidad19" VisibleIndex="26">
                                <Settings AllowHeaderFilter="True" />
                                <SettingsHeaderFilter Mode="CheckedList">
                                </SettingsHeaderFilter>
                            </dx:GridViewDataTextColumn>
                        </Columns>
                        <TotalSummary>
                            <dx:ASPxSummaryItem DisplayFormat="Registros: {0}" FieldName="ID" ShowInColumn="Semana" SummaryType="Count" />
                            <dx:ASPxSummaryItem DisplayFormat="Total: {0}" FieldName="CantidadEtiquetas" ShowInColumn="No. Etiquetas" SummaryType="Sum" />
                        </TotalSummary>
                    </dx:ASPxGridView>
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>

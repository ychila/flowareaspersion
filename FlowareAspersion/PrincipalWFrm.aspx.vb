﻿Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Globalization
Imports System.IO
Imports DevExpress.Web

Public Class PrincipalWFrm

    Inherits System.Web.UI.Page
    Dim Var_NombreArchivo As String = ""
    Dim tmpdsLabelsAspersion As dsLabelsAspersion
    Public FileName As String
    Dim VerificaNombre As Integer = 0

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        InicializarConexion()


        If Not IsPostBack = True Then

            Dim tmpdsLabelsAspersion As New dsLabelsAspersion
            Me.Session("tmpdsRowsBoxLabelsPrints") = tmpdsLabelsAspersion
            Me.lblUsuario.Text = Me.Session("NombreUsr")
            SpeSemana.Value = GetWeekOfYear(Date.Now.ToShortDateString)

        End If
    End Sub

    Public Shared Function GetWeekOfYear(fecha As DateTime) As Integer

        Dim dfi As DateTimeFormatInfo = DateTimeFormatInfo.CurrentInfo
        Dim cal As Calendar = dfi.Calendar

        Return cal.GetWeekOfYear(fecha, dfi.CalendarWeekRule, dfi.FirstDayOfWeek)

    End Function
    Protected Sub btnImportarExcel_Click(sender As Object, e As EventArgs) Handles btnImportarExcel.Click
        Me.ASPxPopupControl2.ShowOnPageLoad = True
    End Sub

    Protected Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Response.Redirect("SplashWFrm.aspx")
    End Sub

    Protected Sub btnSubirExcel_Click(sender As Object, e As EventArgs) Handles btnSubirExcel.Click
        If FileUpload1.HasFile Then
            FileName = Path.GetFileName(FileUpload1.PostedFile.FileName)
            Dim Extension As String = Path.GetExtension(FileUpload1.PostedFile.FileName)
            Dim FolderPath As String = ConfigurationManager.AppSettings("FolderPath")

            Dim FilePath As String = Server.MapPath(FolderPath + FileName)
            FileUpload1.SaveAs(FilePath)
            EliminaCargaAnterior()
            Import_To_Grid(FilePath, Extension, "Yes", Me.Session("TablaCarga"))
            Me.GVAspersion.DataBind()
            Me.SqlDataSourceAspersion.DataBind()
            Me.ASPxPopupControl2.ShowOnPageLoad = False
            Me.SqlDataSourceProductos.DataBind()
            Me.SqlDataSourceCultivos.DataBind()



        End If
    End Sub

    Private Sub EliminaCargaAnterior()

        Dim strsql As String = ""
        strsql = "DELETE " & Me.Session("TablaCarga") & ""
        GetExecComando(strsql, strConexion)

    End Sub

    Private Sub Import_To_Grid(ByVal FilePath As String, ByVal Extension As String, ByVal isHDR As String, ByVal TablaCarga As String)

        Dim conStr As String = ""
        Select Case Extension
            Case ".xls"
                conStr = ConfigurationManager.ConnectionStrings("Excel03ConString") _
                           .ConnectionString
                Exit Select
            Case ".xlsx"
                conStr = ConfigurationManager.ConnectionStrings("Excel07ConString") _
                          .ConnectionString

                Exit Select

        End Select
        conStr = String.Format(conStr, FilePath, isHDR)

        Dim connExcel As New OleDbConnection(conStr)
        Dim cmdExcel As New OleDbCommand()
        Dim oda As New OleDbDataAdapter()
        Dim dt As New DataTable()

        dt.Clear()

        cmdExcel.Connection = connExcel

        connExcel.Open()
        Dim dtExcelSchema As DataTable
        dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
        Dim SheetName As String = dtExcelSchema.Rows(0)("TABLE_NAME").ToString()
        connExcel.Close()

        connExcel.Open()
        cmdExcel.CommandText = "Select * From [" & SheetName & "]"
        oda.SelectCommand = cmdExcel
        oda.Fill(dt)
        connExcel.Close()

        gvImportExcel.Caption = Path.GetFileName(FilePath)
        gvImportExcel.DataSource = dt
        gvImportExcel.DataBind()

        Try

            Dim bulkInsert As New SqlBulkCopy(strConexion)
            bulkInsert.DestinationTableName = TablaCarga
            bulkInsert.WriteToServer(dt)

        Catch ex As Exception

            lblMensajeAlerta.Text = "Inconsistencia con el archivo a cargar, revise el documento."
            Me.ASPxPopupControl3.ShowOnPageLoad = True

        End Try



        Try

            InsertaInfoFinalATablas()

        Catch ex As Exception

            lblMensajeAlerta.Text = "Problemas al registrar la informacion, intente nuevamente."
            Me.ASPxPopupControl3.ShowOnPageLoad = True

        End Try

    End Sub

    Private Sub InsertaInfoFinalATablas()

        VerificaNombreArchivo()

        If VerificaNombre = 0 Then

            Dim strSQL As String = ""

            strSQL = "EXEC sp_InsertaArchivoAspersion " & Me.Session("IDUsr") & ", '" & FileName & "'"
            GetExecComando(strSQL, strConexion)

        Else

            lblMensajeAlerta.Text = "Este archivo ya fue cargado al sistema, renombre el documento."
            ASPxPopupControl3.ShowOnPageLoad = True

        End If




    End Sub

    Private Sub VerificaNombreArchivo()

        Dim strSQL As String = ""

        strSQL = "SELECT COUNT(*) AS Contador FROM Aspersion_Header WHERE IDAppUserRegistro = " & Me.Session("IDUsr") & " AND NombreArchivo = '" & FileName & "'"
        VerificaNombre = GetDatoEscalar(strSQL, strConexion)

    End Sub

    Protected Sub btnCargar_Click(sender As Object, e As EventArgs) Handles btnCargar.Click
        Cargar()
    End Sub
    Private Sub Cargar()
        SqlDataSourceAspersion.ConnectionString = strConexion
        SqlDataSourceAspersion.SelectCommand = "EXEC sp_CargaDatosAspersion " & Me.cmbCultivo.Value & ", '" & Me.SpeSemana.Value & "', '" & cmbDiaSemana.Value & "', '" & cmbProducto.Value & "'"
        GVAspersion.DataBind()
    End Sub

    Protected Sub btnImprimirMasa_Click(sender As Object, e As EventArgs) Handles btnImprimirMasa.Click

        Me.Session("tmpdsRowsBoxLabelsPrints").Clear()

        Var_NombreArchivo = "LPFASP" & Right("00" & Date.Now.Month, 2) & Right("00" & Date.Now.Day, 2) & Right("00" & Date.Now.Hour, 2) & Right("00" & Date.Now.Second, 2) & Right("000" & Me.Session("IDUsr"), 3)

        Dim strPathFileDowLoad As String = ""
        Dim strSQL As String = ""

        strPathFileDowLoad = MapPath("~\Files")

        strSQL = "EXEC sp_PrintLabelAspersion " & cmbCultivo.Value & ", '" & SpeSemana.Value & "','" & cmbDiaSemana.Value & "', 0, " & Me.Session("Printer") & ", '" & cmbProducto.Value & "', " & Me.Session("IDUsr") & ""
        LlenaDataSetGenerico(Me.Session("tmpdsRowsBoxLabelsPrints").Aspersion, "Aspersion", strSQL, strConexion)

        ExportaTabla(Me.Session("tmpdsRowsBoxLabelsPrints").Aspersion, strPathFileDowLoad, "", Var_NombreArchivo, "", ".txt")
        Download("/Files/" & Var_NombreArchivo & ".txt")
    End Sub

    Public Shared Sub Download(patch As String)
        Dim toDownload As New System.IO.FileInfo(HttpContext.Current.Server.MapPath(patch))

        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name)
        HttpContext.Current.Response.AddHeader("Content-Length", toDownload.Length.ToString())
        HttpContext.Current.Response.ContentType = "application/octet-stream"
        HttpContext.Current.Response.WriteFile(patch)
        HttpContext.Current.Response.[End]()
    End Sub

    Private Sub GVAspersion_CustomButtonCallback(sender As Object, e As ASPxGridViewCustomButtonCallbackEventArgs) Handles GVAspersion.CustomButtonCallback
        Dim tmpID As Integer = 0
        Dim tmpSel As Integer
        Dim tmpToPrint As Integer

        On Error GoTo ControlaError

        tmpID = Me.GVAspersion.GetSelectedFieldValues("ID")(0)

        Dim tmpComando As String = ""
        tmpComando = e.ButtonID

        Select Case tmpComando
            Case "Imp"

                Me.Session("tmpdsRowsBoxLabelsPrints").Clear()

                Var_NombreArchivo = "LPFASP" & Right("00" & Date.Now.Month, 2) & Right("00" & Date.Now.Day, 2) & Right("00" & Date.Now.Hour, 2) & Right("00" & Date.Now.Second, 2) & Right("000" & Me.Session("IDUsr"), 3)

                Dim strPathFileDowLoad As String = ""
                Dim strSQL As String = ""

                strPathFileDowLoad = MapPath("~\Files")

                strSQL = "EXEC sp_PrintLabelAspersion 0, '" & SpeSemana.Value & "', 'TODAS'," & tmpID & ", " & Me.Session("Printer") & ", '" & cmbProducto.Value & "', " & Me.Session("IDUsr") & ""
                LlenaDataSetGenerico(Me.Session("tmpdsRowsBoxLabelsPrints").Aspersion, "Aspersion", strSQL, strConexion)

                ExportaTabla(Me.Session("tmpdsRowsBoxLabelsPrints").Aspersion, strPathFileDowLoad, "", Var_NombreArchivo, "", ".txt")
                Download("/Files/" & Var_NombreArchivo & ".txt")
                InsertaLog(tmpID.ToString)

        End Select

        Exit Sub

ControlaError:
        Dim strError As String = Err.Description
    End Sub

    Private Sub InsertaLog(ByVal IDRegistro As String)

        Dim strSQL As String = ""
        Dim Nota As String = "IMprime Producto Individual Registro - " & IDRegistro & ""

        strSQL = "EXEC sp_InsertaLogAspersion " & Me.Session("IDUsr") & ", '" & Nota & "'"
        GetExecComando(strSQL, strConexion)

    End Sub

    Protected Sub btnDocumento_Click(sender As Object, e As EventArgs) Handles btnDocumento.Click
        Me.Response.Redirect("DocumentosWFrm.aspx")
    End Sub
End Class
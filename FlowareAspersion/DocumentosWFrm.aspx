﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="DocumentosWFrm.aspx.vb" Inherits="FlowareAspersion.DocumentosWFrm" %>

<%@ Register assembly="DevExpress.Web.v17.1, Version=17.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type ="text/css">

        #Contenedor
        {
            width: 80%;
            margin: 0 auto;
          
        }
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            width: 49px;
        }
        .auto-style3 {
            width: 180px;
        }
        .auto-style4 {
            width: 42px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div id="Contenedor">
    
        <table class="auto-style1">
            <tr>
                <td>
                    <dx:ASPxMenu ID="ASPxMenu1" runat="server" EnableTheming="True" Theme="Metropolis" Width="100%">
                        <Items>
                            <dx:MenuItem NavigateUrl="~/PrincipalWFrm.aspx" Text="Atras">
                            </dx:MenuItem>
                            <dx:MenuItem NavigateUrl="~/SplashWFrm.aspx" Text="Salir">
                            </dx:MenuItem>
                        </Items>
                    </dx:ASPxMenu>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <table class="dxflInternalEditorTable_Metropolis">
                        <tr>
                            <td class="auto-style2">
                                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="F. Inicial" Theme="Metropolis">
                                </dx:ASPxLabel>
                            </td>
                            <td class="auto-style3">
                                <dx:ASPxDateEdit ID="dtpFini" runat="server" Theme="Metropolis">
                                </dx:ASPxDateEdit>
                            </td>
                            <td class="auto-style4">
                                <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="F. Final" Theme="Metropolis">
                                </dx:ASPxLabel>
                            </td>
                            <td class="auto-style3">
                                <dx:ASPxDateEdit ID="dtpFFin" runat="server" Theme="Metropolis">
                                </dx:ASPxDateEdit>
                            </td>
                            <td>
                                <dx:ASPxButton ID="btnCargar" runat="server" Text="Cargar" Theme="Metropolis" Width="90px">
                                </dx:ASPxButton>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:SqlDataSource ID="SqlDataSourceCargaDocumentos" runat="server" ConnectionString="<%$ ConnectionStrings:Aspersion %>" SelectCommand="EXEC sp_GetInfoAspersionDocument @prmIDUsuario, @prmFini, @prmFFin">
                        <SelectParameters>
                            <asp:SessionParameter DefaultValue="0" Name="prmIDUsuario" SessionField="IDUsr" />
                            <asp:ControlParameter ControlID="dtpFini" DefaultValue="01/01/1900" Name="prmFIni" PropertyName="Value" />
                            <asp:ControlParameter ControlID="dtpFFin" DefaultValue="01/01/1900" Name="prmFFin" PropertyName="Value" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <dx:ASPxGridView ID="gvDocumentos" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceCargaDocumentos" EnableCallBacks="False" EnableTheming="True" KeyFieldName="ID" Theme="SoftOrange" Width="100%">
                        <Settings ShowFooter="True" />
                        <SettingsBehavior AllowHeaderFilter="False" AllowSelectByRowClick="True" AllowSelectSingleRowOnly="True" EnableRowHotTrack="True" />
                        <SettingsSearchPanel Visible="True" />
                        <Columns>
                            <dx:GridViewDataTextColumn FieldName="ID" Visible="False" VisibleIndex="0">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataDateColumn Caption="F. Registro" FieldName="FRegistro" VisibleIndex="3">
                            </dx:GridViewDataDateColumn>
                            <dx:GridViewDataTextColumn FieldName="IDAppUserRegistro" Visible="False" VisibleIndex="5">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="IDAppUserAnulo" Visible="False" VisibleIndex="6">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="Archivo" FieldName="NombreArchivo" VisibleIndex="1">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataCheckColumn Caption="Activo" FieldName="FlagActivo" VisibleIndex="7">
                            </dx:GridViewDataCheckColumn>
                            <dx:GridViewCommandColumn ButtonRenderMode="Image" ButtonType="Image" Caption="Anular" VisibleIndex="8">
                                <CustomButtons>
                                    <dx:GridViewCommandColumnCustomButton ID="Anular">
                                        <Image IconID="actions_close_16x16">
                                        </Image>
                                    </dx:GridViewCommandColumnCustomButton>
                                </CustomButtons>
                            </dx:GridViewCommandColumn>
                            <dx:GridViewDataTimeEditColumn Caption="H. Registro" FieldName="HRegistro" VisibleIndex="4">
                            </dx:GridViewDataTimeEditColumn>
                            <dx:GridViewDataTextColumn FieldName="Usuario" ReadOnly="True" VisibleIndex="2">
                            </dx:GridViewDataTextColumn>
                        </Columns>
                        <TotalSummary>
                            <dx:ASPxSummaryItem DisplayFormat="Registros: {0}" FieldName="FRegistro" ShowInColumn="Archivo" SummaryType="Count" />
                        </TotalSummary>
                    </dx:ASPxGridView>
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>

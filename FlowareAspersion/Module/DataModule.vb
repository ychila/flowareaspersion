﻿Imports System.IO
Imports System.Data.SqlClient

Module DataModule

    Public Sub GetExecComando(ByVal prmSQL As String, ByVal prmStringConeccion As Object)
        Dim strComando As String = ""
        strComando = prmSQL
        Dim cnn As New SqlConnection(prmStringConeccion)
        Dim Comando As New SqlCommand
        Dim tempRsta As Object = Nothing
        Dim Lector As SqlDataReader

        On Error GoTo ControlaError
        With Comando

            .CommandType = CommandType.Text
            .CommandText = prmSQL
            .Connection = cnn
            .CommandTimeout = 0
            cnn.Open()
            Lector = .ExecuteReader(CommandBehavior.CloseConnection)

            If Lector.Read() Then
                tempRsta = Lector.GetValue(0)
            End If
            Lector.Close()


        End With

        cnn.Dispose()
        Comando.Dispose()
        Lector.Dispose()

        Exit Sub
ControlaError:
        Dim DescError As String = Err.Description
    End Sub


    Public Function GetDatoEscalar(ByVal prmSQL As String, ByVal prmStringConeccion As Object) As Object
        Dim strComando As String = ""
        strComando = prmSQL
        Dim cnn As New SqlConnection(prmStringConeccion)
        Dim Comando As New SqlCommand
        Dim tempRsta As Object = Nothing
        Dim Lector As SqlDataReader


        On Error GoTo ControlaError
        With Comando

            .CommandType = CommandType.Text
            .CommandText = prmSQL
            .Connection = cnn
            cnn.Open()
            Lector = .ExecuteReader(CommandBehavior.CloseConnection)

            If Lector.Read() Then
                tempRsta = Lector.GetValue(0)
            End If
            Lector.Close()


        End With

        cnn.Dispose()
        Comando.Dispose()
        Lector.Dispose()

        Return tempRsta

        Exit Function
ControlaError:
        Dim DescError As String = Err.Description

    End Function

    Public Sub ExportaTabla(ByVal prmDataTable As DataTable, ByVal prmDirectorio As String, Optional ByVal prmCamposNO As String = "", Optional ByVal NombreArchivo As String = "", Optional ByVal chrSeparador As String = ",", Optional ByVal prmExtencion As String = ".txt")
        Dim Cursor As DataRow
        Dim tmpNombreArchivo As String = ""
        Dim tmpPathArchivo As String = ""
        Dim tmpStrArchivo As String = ""
        Dim tmpLinea As String = ""
        On Error GoTo ControlaError
        If NombreArchivo.Length > 0 Then
            tmpNombreArchivo = NombreArchivo
        Else
            tmpNombreArchivo = prmDataTable.TableName
        End If
        tmpPathArchivo = prmDirectorio & "\" & tmpNombreArchivo & prmExtencion
        Dim Escritor As New StreamWriter(tmpPathArchivo, False)

        Dim i As Integer = 0
        Dim j As Integer = 0

        Dim CursorCampos As DataColumn
        For Each Cursor In prmDataTable.Rows
            If Cursor.RowState <> DataRowState.Deleted Then
                tmpLinea = ""
                For Each CursorCampos In prmDataTable.Columns
                    If Not (prmCamposNO.Contains("," & CursorCampos.ColumnName & ",")) And Not (prmCamposNO.Contains("," & CursorCampos.ColumnName)) And Not (prmCamposNO.Contains(CursorCampos.ColumnName)) Then
                        If i = 0 Then
                            tmpLinea = Cursor.Item(CursorCampos.ColumnName)

                        Else
                            tmpLinea = tmpLinea & chrSeparador & Cursor.Item(CursorCampos.ColumnName)

                        End If
                        i = i + 1
                    Else
                        Dim a = "ris"
                    End If

                Next
                i = 0
                Escritor.WriteLine(tmpLinea)
                
            End If

        Next

        Escritor.Close()
        Exit Sub
ControlaError:
        Dim strError As String = Err.Description
    End Sub


    Public Sub ExportaTabla2(ByVal prmDataTable As DataTable, ByVal prmDirectorio As String, Optional ByVal prmCamposNO As String = "", Optional ByVal NombreArchivo As String = "", Optional ByVal chrSeparador As String = ",", Optional ByVal prmExtencion As String = ".txt")
        Dim Cursor As DataRow
        Dim tmpNombreArchivo As String = ""
        Dim tmpPathArchivo As String = ""
        Dim tmpStrArchivo As String = ""
        Dim tmpLinea As String = ""
        On Error GoTo ControlaError
        If NombreArchivo.Length > 0 Then
            tmpNombreArchivo = NombreArchivo
        Else
            tmpNombreArchivo = prmDataTable.TableName
        End If
        tmpPathArchivo = prmDirectorio & "\" & tmpNombreArchivo & prmExtencion
        Dim Escritor As New StreamWriter(tmpPathArchivo)

        Dim i As Integer = 0
        Dim j As Integer = 0

        Dim CursorCampos As DataColumn
        For Each Cursor In prmDataTable.Rows
            If Cursor.RowState <> DataRowState.Deleted Then
                tmpLinea = ""
                For Each CursorCampos In prmDataTable.Columns
                    If Not (prmCamposNO.Contains("," & CursorCampos.ColumnName & ",")) Then
                        If i = 0 Then
                            tmpLinea = Cursor.Item(CursorCampos.ColumnName)

                        Else
                            tmpLinea = tmpLinea & chrSeparador & Cursor.Item(CursorCampos.ColumnName)

                        End If
                        i = i + 1
                    Else
                        Dim a = "ris"
                    End If

                Next
                i = 0
                Escritor.WriteLine(tmpLinea)

            End If

        Next

        Escritor.Close()
        Exit Sub
ControlaError:
        Dim strError As String = Err.Description
    End Sub

    
End Module

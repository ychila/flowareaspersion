﻿Imports System.Data.SqlClient

Public Class SendEmail

    Private Attach As Net.Mail.Attachment
    Private Ruta As String
    Private Message As New System.Net.Mail.MailMessage
    Private SMTP As New System.Net.Mail.SmtpClient
    Dim Email As String
    Dim Contrasenia As String
    Dim CorreosEnviar As String

    Public Sub New(ByVal Finca As String, ByVal FechaPedido As String, ByVal Order As Integer)

        GetDatosEmail()


        SMTP.Credentials = New System.Net.NetworkCredential(Email, Contrasenia)
        SMTP.Host = "mail.floreskatama.com"
        SMTP.Port = 587
        SMTP.EnableSsl = False


        Message.[To].Add(CorreosEnviar)
        Message.From = New System.Net.Mail.MailAddress(Email, "Revisor UPC", System.Text.Encoding.UTF8)
        Message.Subject = "Imagen UPC para Revision"
        Message.SubjectEncoding = System.Text.Encoding.UTF8
        Message.Body = "Buen dia, " & vbCrLf & " " & vbCrLf & "Finca " & Finca & " realizo carga de imagen al sistema para su revision y aprobacion." & vbCrLf & "" & vbCrLf & "Numero de Orden: " & Order & " " & vbCrLf & "Farm Ship: " & FechaPedido & " " & vbCrLf & " " & vbCrLf & "Gracias. " & vbCrLf & "Sistema Web Fincas."
        Message.BodyEncoding = System.Text.Encoding.UTF8
        Message.Priority = System.Net.Mail.MailPriority.Normal
        Message.IsBodyHtml = False

        Try
            SMTP.Send(Message)
        Catch ex As System.Net.Mail.SmtpException

        End Try


    End Sub

    Private Sub GetDatosEmail()

        Dim strSQL As String = ""
        strSQL = "EXEC ParametrosCorreoFotosUPC"

        Dim cnn As New SqlConnection(strConexion)
        Dim Comando As New SqlCommand
        Dim Lector As SqlDataReader

        With Comando
            .Connection = cnn
            .Connection.Open()
            .CommandText = strSQL
            .CommandTimeout = 0
            .CommandType = CommandType.Text
            Lector = .ExecuteReader(CommandBehavior.CloseConnection)

            If Lector.Read Then
                Email = Lector.GetValue(0)
                Contrasenia = Lector.GetValue(1)
                CorreosEnviar = Lector.GetValue(2)
            End If
            Lector.Close()

        End With

    End Sub


End Class
﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SplashWFrm.aspx.vb" Inherits="FlowareAspersion.SplashWFrm" %>

<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="css/bootstrap.css" rel="stylesheet"/>
    <link href="css/Styles.css" rel="stylesheet"/>
    <script src="js/bootstrap.min.js"></script>
    <style type="text/css">
        .Image-group2
        {
            text-align: center;
        }
        .auto-style2
        {
            height: 91px;
            width: 173px;
        }
        </style>
</head>
<body>
    <div class ="container" >
    <form id="form1" runat="server" class ="FormPrincipal">
    <div class="Image-group">

        <img class="auto-style2" src="Images/Logo.jpg" />

    </div>

    <div class="Image-group2">
        <asp:Label runat ="server" ID ="Label4" Text ="Programa de Aspersión"></asp:Label>
    </div>

    <div class="form-group">
    <asp:Label runat ="server" ID ="Label1" Text ="Usuario"></asp:Label>
    <asp:TextBox runat="server"  class="form-control" id="txtLogin" aria-describedby="emailHelp" />
    <asp:Label runat ="server" ID ="Label2" Text ="Contraseña"></asp:Label>
        <asp:TextBox runat="server"  class="form-control" id="txtPassword" aria-describedby="emailHelp" TextMode="Password" />
    </div> 
               
        <asp:Button ID="btnIngresar" class="btn btn-primary btn-Login "  runat="server" Text="Ingresar" />
        
        
       
    </form>
     </div>

</body>
</html>
